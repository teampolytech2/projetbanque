# README #

This README would normally document whatever steps are necessary to get your application up and running.

##Production de la Javadoc

La Javadoc est un outil permettant de creer une documentation des classes Java.
Elle est generee à partir de certains blocs de commentaires et tags. 
Les blocs de commentaire commencent par `\**`. Ces blocs se placent avant les declarations des classes, des methodes et d'attributs.
Les tags commencent par un arobase `@` (Exemple de tags : `@param` ou `@return`).


### En ligne de commande
Pour produire la Javadoc d'une classe à partir d'une ligne de commande, il faut utiliser la commande :
```{bash}
javadoc NomClasse.java -d NomDossier
```


### Sous Eclipe
Pour produire la Javadoc d'une classe sous Eclipse, il faut suivre le chemin : `Project > Generate Javadoc`. 


## Generer l'archive executable .jar

1. Generer tous les fichier .class avec la commande 
```{bash}
javac *.java
```
2. Generer l'archive executable "Programme.jar" avec la commande
```{bash}
jar cvmf MANIFEST.MF Programme.jar *.class
```


## Executer l'archive executable
* : Utiliser la commande "java -jar Programme.javar".
