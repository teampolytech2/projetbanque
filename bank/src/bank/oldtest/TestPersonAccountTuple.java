package bank.oldtest;
import bank.main.*;

public class TestPersonAccountTuple {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Person homme1 = new Person("AITABBI", "Sami", 1993, 1, 19);
		Person homme2 = new Person("AZERTY", "Sam", 1996, 1, 19);
		
		Account compte1 = new Account("0265545", 2500);
		Account compte2 = new Account("0265546", 1620);
		Account compte3 = new Account("0265547", 1620);
		
		PersonAccountTuple pat1 = new PersonAccountTuple(homme1);
		pat1.addAccount(compte1);
		pat1.addAccount(compte2);
		pat1.addAccount(compte3);
		System.out.println(pat1.hasPersonAccount());
		System.out.println(pat1.toString());
		
		pat1.deleteAccount(compte3);
		System.out.println(pat1.hasPersonAccount());
		System.out.println(pat1.toString());
		
		pat1.deleteAllAccounts();
		System.out.println(pat1.hasPersonAccount());
		System.out.println(pat1.toString());
		
		pat1.addAccount(compte1);
		pat1.addAccount(compte2);
		
		System.out.println(pat1.ownedByPerson(homme2));
		System.out.println(pat1.balance());
		Account[] listeDeComptes= pat1.getAccounts();
		for (int i = 0; i < listeDeComptes.length ; i+= 1) {
			System.out.println(listeDeComptes[i].toString());
			
		}
		
		
	}

}

