package bank.oldtest;
import bank.main.*;

public class ForeignCurrencyAccountTest {
	
	public static void main (String[] args) {
		ForeignCurrencyAccount  compte1 = new ForeignCurrencyAccount("89654", 8000, 0.9);
		compte1.deposit(2000);
		compte1.withdraw(5000);
		System.out.println(compte1.balance());
		System.out.println(compte1.accountNumber());
		compte1.withdraw(6000);
		System.out.println(compte1.toString());
		
		
		System.out.println(compte1.getExchangeRate());
		System.out.println(compte1.balanceForeignCurrency());
		compte1.depositForeignCurrency(5000);
		compte1.withdrawForeignCurrency(20000);
		System.out.println(compte1.toString());
		compte1.withdrawForeignCurrency(200);
		System.out.println(compte1.toString());
		
		
		
	}

}
