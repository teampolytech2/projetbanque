package bank.oldtest;
import bank.main.*;

public class AccountTest {
	
	public static void main (String[] args) {
		Account compte1 = new Account("89654", 8000);
		compte1.deposit(2000);
		compte1.withdraw(5000);
		System.out.println(compte1.balance());
		System.out.println(compte1.accountNumber());
		compte1.withdraw(6000);
		System.out.println(compte1.toString());
		
	}

}
