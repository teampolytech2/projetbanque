package bank.oldtest;
import bank.main.*;

public class TestAccountManager {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Person homme1 = new Person("AITABBI", "Sami", 1993, 1, 19);
		Person homme2 = new Person("AZERTY", "Sam", 1996, 1, 19);
		
		Account compte1 = new Account("0265545", 2500);
		Account compte2 = new Account("0265546", 1620);
		Account compte3 = new Account("0265547", 1620);
		
		
		AccountManager am = new AccountManager();
		am.manageAccount(homme1, compte1);
		System.out.println(am.toString() + "\n Nombre de personnes : " + am.getNumberOfPersons()
				+ "\n Nombre de comptes : " + am.getNumberOfAccounts()
				+ "\n Balance : " + am.balance() + "\n");
		
		am.manageAccount(homme1, compte2);
		am.manageAccount(homme2, compte3);
		System.out.println(am.toString() + "\n Nombre de personnes : " + am.getNumberOfPersons()
				+ "\n Nombre de comptes : " + am.getNumberOfAccounts()
				+ "\n Balance : " + am.balance() + "\n");
		
		am.unmanageAccount(homme1, compte2);
		am.unmanageAccount(homme1, compte3);
		System.out.println(am.toString() + "\n Nombre de personnes : " + am.getNumberOfPersons()
				+ "\n Nombre de comptes : " + am.getNumberOfAccounts()
				+ "\n Balance : " + am.balance() + "\n");
		
		
		am.unmanagePerson(homme2);
		System.out.println(am.toString() + "\n Nombre de personnes : " + am.getNumberOfPersons()
				+ "\n Nombre de comptes : " + am.getNumberOfAccounts()
				+ "\n Balance : " + am.balance() + "\n");
		
		
	}

}