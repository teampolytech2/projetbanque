package bank.oldtest;
import bank.main.*;

public class TestPerson {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Person homme1 = new Person("AITABBI", "Sami", 1993, 1, 19);
		System.out.println(homme1.toString());
		System.out.println("Age : " + homme1.getAge());
		System.out.println("Nom : " + homme1.getName());
		System.out.println("Prénom : " + homme1.getSurname());
		
		Person homme2 = new Person("HERAULT", "Leo", 1993, 11, 19);
		System.out.println(homme2.toString());
		System.out.println("Age : " + homme2.getAge());
		System.out.println("Nom : " + homme2.getName());
		System.out.println("Prénom : " + homme2.getSurname());
		
	}

}
