package bank.oldtest;
import bank.main.*;

public class LimitedAccountTest {
	
	public static void main (String[] args) {
		LimitedAccount compte1 = new LimitedAccount("89654", 8000, 200);
		compte1.deposit(2000);
		compte1.withdraw(5000);
		System.out.println(compte1.balance());
		System.out.println(compte1.accountNumber());
		compte1.withdraw(5100);
		System.out.println(compte1.toString());
		
		System.out.println(compte1.getMaxCredit());
		compte1.setMaxCredit(300);
		System.out.println(compte1.getMaxCredit());
		compte1.withdraw(250);
		System.out.println(compte1.toString());
		
	}
		
		
		

}
