package bank.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import bank.main.Account;
import bank.main.LimitedAccount;
import bank.main.Person;
import bank.main.PersonAccountTuple;

public class TestPersonAccountTuple {


	@BeforeClass
	public static void oneTimeSetUp() {
		// code d'initialisation appelé automatiquement par junit une seule fois au tout début (pour les tests compliqués où l'on doit initialiser des variables STATIC communes à tous les tests, ouvrir des fichiers, etc.)   

	}

	@AfterClass
	public static void oneTimeTearDown() {
		//  code de nettoyage appelé automatiquement par junit une seule fois à la toute fin (fermeture des fichiers, etc.)

	}

	@Before
	public void setUp() {
		// initialisation : méthode appelée automatiquement avant chaque test
	}

	@After
	public void tearDown() {
		// nettoyage : méthode appelée automatiquement après chaque test
	}

	// ma longue liste de tests suit
	@Test
	public void testOwnedPerson() {
		Person p = new Person("Jesus", "De Nazareth", -7, 12, 25);
		PersonAccountTuple pat = new PersonAccountTuple(p);
		Assert.assertTrue(pat.ownedByPerson(p));
	}
	
	@Test
	public void testAjoutComptes() {
		Person p = new Person("Jesus", "De Nazareth", -7, 12, 25);
		PersonAccountTuple pat = new PersonAccountTuple(p);
		Account a  = new Account("1234", 50.0);
		pat.addAccount(a);
		Assert.assertTrue(pat.getAccounts().length == 1);
		Assert.assertTrue(pat.balance() == 50.0);
	}
	
	@Test
	public void testDeleteAccounts() {
		Person p = new Person("Jesus", "De Nazareth", -7, 12, 25);
		PersonAccountTuple pat = new PersonAccountTuple(p);
		Account a  = new Account("1234", 50.0);
		Account b  = new Account("12345", 60.0);
		pat.addAccount(a);
		pat.addAccount(b);
		Assert.assertTrue(pat.balance() == 110.0);
		pat.deleteAccount(b);
		Assert.assertTrue(pat.getAccounts().length == 1);
		Assert.assertTrue(pat.balance() == 50.0);
	}
	
	@Test
	public void testDeleteAllAccounts() {
		Person p = new Person("Jesus", "De Nazareth", -7, 12, 25);
		PersonAccountTuple pat = new PersonAccountTuple(p);
		Account a  = new Account("1234", 50.0);
		Account b  = new Account("12345", 60.0);
		pat.addAccount(a);
		pat.addAccount(b);
		Assert.assertTrue(pat.balance() == 110.0);
		pat.deleteAllAccounts();
		Assert.assertFalse(pat.hasPersonAccount());
	}
}
