package bank.test;

import org.junit.*;
import org.junit.Assert.*;

import bank.main.Person;


public class TestPerson {

	@BeforeClass
	public static void oneTimeSetUp() {
		// code d'initialisation appelé automatiquement par junit une seule fois au tout début (pour les tests compliqués où l'on doit initialiser des variables STATIC communes à tous les tests, ouvrir des fichiers, etc.)   

	}

	@AfterClass
	public static void oneTimeTearDown() {
		//  code de nettoyage appelé automatiquement par junit une seule fois à la toute fin (fermeture des fichiers, etc.)

	}

	@Before
	public void setUp() {
		// initialisation : méthode appelée automatiquement par junit avant chaque test
	}

	@After
	public void tearDown() {
		// nettoyage : méthode appelée automatiquement par junit après chaque test
	}

	// ma longue liste de tests suit
	@Test
	public void testPersonNameSurname() {
		Person p = new Person("Jesus", "De Nazareth", -7, 12, 25);
		Assert.assertTrue(p.getName().equals("Jesus"));
		Assert.assertFalse(p.getSurname().equals("Pamela"));
	}
	
	public void testPersonAge(){
		Person p = new Person("Sami", "Devin", 1990, 12, 25);
		Assert.assertFalse(p.getAge() == 1990);
	}
}
