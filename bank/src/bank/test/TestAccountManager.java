package bank.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import bank.main.Account;
import bank.main.AccountManager;
import bank.main.Person;
import bank.main.PersonAccountTuple;

public class TestAccountManager {

	@BeforeClass
	public static void oneTimeSetUp() {
		// code d'initialisation appelé automatiquement par junit une seule fois au tout début (pour les tests compliqués où l'on doit initialiser des variables STATIC communes à tous les tests, ouvrir des fichiers, etc.)   

	}

	@AfterClass
	public static void oneTimeTearDown() {
		//  code de nettoyage appelé automatiquement par junit une seule fois à la toute fin (fermeture des fichiers, etc.)

	}

	@Before
	public void setUp() {
		// initialisation : méthode appelée automatiquement avant chaque test
	}

	@After
	public void tearDown() {
		// nettoyage : méthode appelée automatiquement après chaque test
	}

	@Test
	public void testAjoutPerson() {
		Person p = new Person("Jesus", "De Nazareth", -7, 12, 25);
		Account a  = new Account("1234", 50.0);
		Account b  = new Account("12345", 60.0);
		AccountManager am = new AccountManager();
		am.manageAccount(p, a);
		Assert.assertTrue(am.getAccounts(p).length == 1);
	}
	
	@Test
	public void testDeleteAccount() {
		Person p = new Person("Jesus", "De Nazareth", -7, 12, 25);
		Account a  = new Account("1234", 50.0);
		Account b  = new Account("12345", 60.0);
		AccountManager am = new AccountManager();
		am.manageAccount(p, a);
		am.manageAccount(p, b);
		Assert.assertTrue(am.getNumberOfAccounts() == 2);
		Assert.assertTrue(am.balance() == 110.0);
		Assert.assertTrue(am.unmanageAccount(p, b));
		Assert.assertTrue(am.getNumberOfAccounts() == 1);
		Assert.assertTrue(am.balance() == 50.0);
	}
	
	@Test
	public void testDeletePerson() {
		Person p = new Person("Jesus", "De Nazareth", -7, 12, 25);
		Account a  = new Account("1234", 50.0);
		Account b  = new Account("12345", 60.0);
		AccountManager am = new AccountManager();
		am.manageAccount(p, a);
		am.manageAccount(p, b);
		Assert.assertTrue(am.getAccounts(p).length == 2);
		Assert.assertTrue(am.unmanagePerson(p));
		Assert.assertTrue(am.getNumberOfPersons() == 0);
	}
}
