package bank.test;

import org.junit.*;
import org.junit.Assert.*;

import bank.main.ForeignCurrencyAccount;
import bank.main.Person;

public class TestForeignCurrencyAccount {
 	


    @BeforeClass
    public static void oneTimeSetUp() {
        // code d'initialisation appelé automatiquement par junit une seule fois au tout début (pour les tests compliqués où l'on doit initialiser des variables STATIC communes à tous les tests, ouvrir des fichiers, etc.)   
        
    }
 
    @AfterClass
    public static void oneTimeTearDown() {
        //  code de nettoyage appelé automatiquement par junit une seule fois à la toute fin (fermeture des fichiers, etc.)
        
    }
 
    @Before
    public void setUp() {
                // initialisation : méthode appelée automatiquement avant chaque test
    }
 
    @After
    public void tearDown() {
                // nettoyage : méthode appelée automatiquement après chaque test
    }
 
        // ma longue liste de tests suit
	@Test
    public void testBalanceForeignCurrency() {
                ForeignCurrencyAccount a  = new ForeignCurrencyAccount("1234", 50.0, 1.3);
        		Assert.assertTrue(a.balanceForeignCurrency() == 65);
				
    }
    
	@Test
    public void testDepositForeignCurrency() {
                ForeignCurrencyAccount a  = new ForeignCurrencyAccount("1234", 50.0, 1.3);
        		a.depositForeignCurrency(50);
				Assert.assertTrue(a.balance() == (50.0 + (50*1.3)));
    }
	@Test
    public void testWithdrawForeignCurrencyMax() {
		ForeignCurrencyAccount a = new ForeignCurrencyAccount("1234", 50.0, 1.3);
			Assert.assertTrue(a.withdrawForeignCurrency(65));
				Assert.assertTrue(a.balance() == 0 );
    }
	@Test
    public void testWithdrawForeignCurrencyTooMuch() {
		ForeignCurrencyAccount a = new ForeignCurrencyAccount("1234", 50.0, 1.3);
			Assert.assertFalse(a.withdrawForeignCurrency(100));
				Assert.assertTrue(a.balance() == 50.0 );
    }

	
}
