package bank.test;

import org.junit.*;
import org.junit.Assert.*;

import bank.main.Account;
import bank.main.LimitedAccount;
import bank.main.Person;

public class TestLimitedAccount {
 	
    @BeforeClass
    public static void oneTimeSetUp() {
        // code d'initialisation appelé automatiquement par junit une seule fois au tout début (pour les tests compliqués où l'on doit initialiser des variables STATIC communes à tous les tests, ouvrir des fichiers, etc.)   
        
    }
 
    @AfterClass
    public static void oneTimeTearDown() {
        //  code de nettoyage appelé automatiquement par junit une seule fois à la toute fin (fermeture des fichiers, etc.)
        
    }
 
    @Before
    public void setUp() {
                // initialisation : méthode appelée automatiquement avant chaque test
    }
 
    @After
    public void tearDown() {
                // nettoyage : méthode appelée automatiquement après chaque test
    }
 
        // ma longue liste de tests suit
	
	@Test
    public void testWithdrawMax() {
		LimitedAccount a = new LimitedAccount("1234", 50.0, 20);
			Assert.assertTrue(a.withdraw(70));
				Assert.assertTrue(a.balance() == (50.0 - 70) );
    }
	@Test
    public void testWithdrawTooMuch() {
		LimitedAccount a = new LimitedAccount("1234", 50.0, 30);
		Assert.assertFalse(a.withdraw(90));
		Assert.assertTrue(a.balance() == 50.0 );
    }

	
}
