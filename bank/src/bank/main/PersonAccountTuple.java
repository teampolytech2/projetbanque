/**
 * Classe liant une personne à un ensemble de comptes
 * @author Sami
 */

package bank.main;

import java.util.ArrayList;


public class PersonAccountTuple {
	//Variables d'instances
	private Person personne;
	private ArrayList<Account> comptes;
	
	/**
	 * Constructeur de l'objet PersonAccountTuple
	 * @param Objet Personne
	 */
	public PersonAccountTuple(Person personne){
		this.personne = personne;
		this.comptes = new ArrayList<Account>();
	}
	
	/**
	 * Ajouter un compte pour l'objet PersonAccountTuple
	 * @param Compte à ajouter
	 */
	public void addAccount(Account compte){
		if(comptes.contains(compte) == false){
			this.comptes.add(compte);
		}
	}
	
	/**
	 * Supprime un compte pour l'objet PersonAccontTuple
	 * @param Compte à supprimer
	 * @return true si le compte est supprimé et false dans le cas inverse
	 */
	public boolean deleteAccount(Account compte){
		if(this.comptes.contains(compte)){
			this.comptes.remove(compte);
			return true;
		}
		else
			return false;
	}
	
	/**
	 * Supprime tous les comptes associés à une personne
	 */
	public void deleteAllAccounts(){
		this.comptes.clear();
	}
	
	/**
	 * Donne la liste de tous les comptes d'un personne
	 * @return Liste des comptes (objet Account)
	 */
	public Account[] getAccounts(){
		Account[] liste = new Account[this.comptes.size()];
		for(int i = 0; i < liste.length; i++){
			liste[i] = this.comptes.get(i);
		}
		return liste;
	}
	
	/**
	 * Retourne la personne concerné par l'objet PersonAccountTuple
	 * @return Objet Person
	 */
	public Person getPerson(){
		return this.personne;
	}
	
	/**
	 * Vérifie si la personne a un compte
	 * @return true si la personne a un compte et false si elle n'a pas de compte
	 */
	public boolean hasPersonAccount(){
		if(comptes.isEmpty())
			return false;
		else
			return true;
	}
	
	/**
	 * Vérifie si la personne indiqué est la même que celle concerné par l'objet PersonAccountTuple
	 * @param Personne à vérifier
	 * @return true si la personne indiquée est identique à celle du PersonAccountTuple, false dans le cas inverse
	 */
	public boolean ownedByPerson(Person p){
		if(p.equals(this.personne))
			return true;
		else
			return false;	
	}
	
	/**
	* Calcul de la somme des balances de tous les comptes gérés d'une même personne
	* @return Balance de tous les comptes d'une même personne
	*/
	public double balance(){
		double balance = 0;
		for(Account c : comptes){
			balance += c.balance();
		}
		return balance;
	}
	
	/**
	 * Affichage textuelle de l'objet PersonAccountTuple (avec la personne et ses comptes)
	 * @return Représentation textuelle de l'objet PersonAccountTuple
	 */
	public String toString(){
		String expression = this.personne.toString();
		for(Account c : comptes){
			expression += "\n\t" + c.toString();
		}
		return expression;
	}
}
