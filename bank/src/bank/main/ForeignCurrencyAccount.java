package bank.main;

public class ForeignCurrencyAccount extends Account {

	//Variable d'instance 
	private double tauxDechange;
	
	//Constructeur
	
	/**
	 * Constructeur
	 * @param numeroDeCompte
	 * @param montantInitial
	 * @param tauxDechange
	 */
	
	public ForeignCurrencyAccount(String numeroDeCompte, double montantInitial,
			double tauxDechange) {
		super(numeroDeCompte, montantInitial);
		this.tauxDechange = tauxDechange;
	}
	
	
	/**
	 * Retourne la solde en devise etrangere
	 * @return solde en devise étrangère
	 */
	
	public double balanceForeignCurrency() {
		return this.balance()*this.tauxDechange;
	}
	
	public double depositForeignCurrency(double depotDeviseEtrangere) {
			return this.solde += depotDeviseEtrangere*(this.tauxDechange);
		
		
	}
	/**
	 * Obtenir le taux de change
	 * @return taux de change
	 */
	public double getExchangeRate() {
		return this.tauxDechange;
	}
	
	
	/**
	 * 
	 * @param retrait
	 * @return true si le montant stocke est suffisant, false dans le cas contraire
	 */
	
	public boolean withdrawForeignCurrency(double retraitDeviseEtrangere) {
		if (this.balanceForeignCurrency() >= retraitDeviseEtrangere) {
			this.solde -= retraitDeviseEtrangere/(this.tauxDechange);
			return true;			
		} else {
			return false;
		}
	}

	/**
	 * Obtenir les informations sur le compte
	 * @return Numero de compte et montant stocke dans les deux devises
	 */
	public String toString() {
		return "NumeroDeCompte : " + this.accountNumber()
					+ ", montant stocké :" + solde + " euros " + "soit " 
						+ this.balanceForeignCurrency() + "$";
	}
	
	
	
	
	

	
	
	
	
	
	

}
