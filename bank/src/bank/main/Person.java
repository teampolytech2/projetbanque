package bank.main;

import java.util.Calendar;

/**
 * Classe de gestion de personnes
 * @author Sami
 */
public class Person {

	//Variables d'instance
	private String name, surname;
	private int birthYear, birthMonth, birthDay;
	
	/**
	 * Constructeur de l'objet Person
	 * @param name
	 * @param surname
	 * @param birthYear
	 * @param birthMonth
	 * @param birthDay
	 */
	public Person(String name, String surname, int birthYear, int birthMonth, int birthDay){
		if (birthMonth < 1 || birthMonth > 12 || birthDay < 1 || birthDay > 31 
                || birthDay == 31 && (birthMonth == 4 || birthMonth == 6 || birthMonth == 9 || birthMonth == 11)
                || birthDay == 30 && birthMonth == 2 
                || birthDay == 29 && birthMonth == 2 && birthYear % 4 != 0) {
            System.out.println("Date incorrecte");
            System.exit(0);
		}
		this.name = name;
		this.surname = surname;
		this.birthYear = birthYear;
		this.birthMonth = birthMonth;
		this.birthDay = birthDay;
	}
	
	/**
	 * Retourne l'age de la personne
	 * @return Age de la personne
	 */
	public int getAge(){
		Calendar now = Calendar.getInstance();
		if((this.birthMonth > now.get(Calendar.MONTH)) || 
				(this.birthMonth == now.get(Calendar.MONTH) && 
				this.birthDay > now.get(Calendar.DAY_OF_MONTH)))
			return now.get(Calendar.YEAR) - this.birthYear - 1 ;
		else
			return now.get(Calendar.YEAR) - this.birthYear;
	}
	
	/**
	 * Retourne le prénom de la personne
	 * @return Prénom de la personne
	 */
	public String getSurname(){
		return this.surname;
	}
	
	
	/**
	 * Retourne le nom de la personne
	 * @return Nom de la personne
	 */
	public String getName(){
		return this.name;
	}
	
	/**
	 * Affichage textuelle de l'objet Person (avec nom et date de naissance)
	 * @return Représentation textuelle de la personne
	 */
	public String toString(){
		return this.name.toUpperCase() + " " + this.surname + " né le " +
				this.birthDay + "/" + this.birthMonth + "/" + this.birthYear;
	}

}
