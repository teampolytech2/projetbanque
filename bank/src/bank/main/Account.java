package bank.main;

public class Account {
	
	//Variables d'instance
	
	private String numeroDeCompte;
	protected double solde;
	
	
	/**
	 * Constructeur
	 * @param numeroDeCompte
	 * @param montantInitial
	 */
	
	
	public Account(String numeroDeCompte, double montantInitial) {
		
		this.numeroDeCompte = numeroDeCompte;
		this.solde = montantInitial;
	}

	/**
	 * Numero de compte
	 * @return numéro de compte
	 */
	public String accountNumber() {
		return this.numeroDeCompte;
	}
	
	
	/**
	 * Solde
	 * @return montant stocke
	 */
	
	public double balance() {
		
		return this.solde;
	}
	
	
	/**
	 * Dépôt
	 * Ajouter de l'argent sur le compte
	 * @param depot
	 */
	
	
	public void deposit(double depot) {
		
		this.solde += depot;
	}
	/**
	 * Effectuer un retrait
	 * Soustrait au montant stocké la valeur du retrait
	 * @param retrait
	 * @return true si le montant stocke est suffisant, false dans le cas contraire
	 * 
	 */
	
	//
	
	public boolean withdraw(double retrait) {
		
		if (this.solde >= retrait) {
			this.solde -= retrait;
			return true;	
		} else {
			return false;
		}
	}
	
	
	/**
	 * Afficher les informations du compte
	 * @return Numero de compte et montant stocke
	 */
	

	public String toString() {
		return "NumeroDeCompte : " + numeroDeCompte
				+ ", montant stocké :" + solde + " euros";
	}
	
	

}
