package bank.main;

public class LimitedAccount extends Account {
	
	private double maxCredit;
	
	/**
	 * constructeur
	 * @param numeroDeCompte
	 * @param montantInitial
	 * @param maxCredit
	 */

	public LimitedAccount(String numeroDeCompte, double montantInitial,
			double maxCredit) {
		super(numeroDeCompte, montantInitial);
		this.maxCredit = maxCredit;
	}
	/**
	 * Obtenir le credit maximum
	 * @return credit max
	 */
	
	public double getMaxCredit() {
		return this.maxCredit;
	}
	
	/**
	 * Fixer le credit max
	 * @param maxCredit
	 */
	
	public void setMaxCredit(double maxCredit) {
		this.maxCredit = maxCredit;
	}
	/**
	 * Effectuer un retrait 
	 * @return true si la somme (montant stocke + credit max)  est suffisante, false dans le cas contraire
	 */
	
	public boolean withdraw(double retrait) {
		if ((this.solde + this.maxCredit) >= retrait) {
			this.solde -= retrait;
			return true;
		} else {
			return false;
		}
	}
	
	public String toString() {
		return "NumeroDeCompte : " + this.accountNumber()
				+ ", montant stocké :" + solde + " euros; " + "crédit max : "
				+ this.getMaxCredit() + " euros";
	}
	

	
	
	
	

}
