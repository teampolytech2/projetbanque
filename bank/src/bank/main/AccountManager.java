package bank.main;

import java.util.ArrayList;

/**
 * Classe de gestion de PersonAccountTuple
 * @author Sami
 */
public class AccountManager {
	//Variables d'instances
	private ArrayList<PersonAccountTuple> listPerson;
	
	/**
	 * Constructeur de la classe AccountManager
	 */
	public AccountManager(){
		this.listPerson = new ArrayList<PersonAccountTuple>();
	}
	
	/**
	 * Ajoute (si c'est possible) la persone et le compte concerné du portefeuille gérée par l'AccountManager
	 * @param Personne à ajouter
	 * @param Compte à ajouter
	 * @return vrai si le compte et la personne désigné est présent dans le portefeuille de l'AccountManager et false dans le cas inverse
	 */
	public boolean manageAccount(Person personne, Account compte){
		boolean gestion = false;
		if(listPerson.isEmpty() == false){
			for(PersonAccountTuple pat : this.listPerson){
				if(pat.getPerson().equals(personne)){
					gestion = true;
					pat.addAccount(compte);
					return gestion;
				}
			}
		}
		if(gestion == false){
			PersonAccountTuple ajout = new PersonAccountTuple(personne);
			ajout.addAccount(compte);
			listPerson.add(ajout);
		}
		return gestion;
	}
	
	/**
	 * Supprime (si c'est possible) la persone et le compte concerné du portefeuille gérée par l'AccountManager
	 * @param Personne à supprimer
	 * @param Compte à supprimer
	 * @return Vrai si la personne et le compte indiqué est supprimé de l'objet AccountManager et false dans le cas inverse
	 */
	public boolean unmanageAccount(Person personne, Account compte){
		boolean gestion = false;
		for(PersonAccountTuple pat : this.listPerson){
			if(pat.getPerson().equals(personne)){
				gestion = true;
				return pat.deleteAccount(compte);
			}
		}
		return gestion;	
	}
	
	/**
	 * Supprime (si c'est possible) la persone et tous ses comptes associés du portefeuille gérée par l'AccountManager 
	 * @param Personne à supprimer
	 * @return si la personne est supprimé de l'objet AccountManager et false dans le cas inverse
	 */
	public boolean unmanagePerson(Person personne){
		for(PersonAccountTuple pat : this.listPerson){
			if(pat.getPerson().equals(personne)){
				return listPerson.remove(pat);
			}
		}
		return false;
	}
	
	/**
	 * Obtenir la liste des comptes d'une personne gérée par l'AccountManager
	 * @return la liste d'objets Account
	 */
	public Account[] getAccounts(Person personne){
		boolean test = false;
		for(PersonAccountTuple pat : this.listPerson){
			if(pat.getPerson().equals(personne)){
				test = true;
				return pat.getAccounts();
			}
		}
		return null;	
	}
	
	/**
	* Calcul de la balance de tous les comptes gérés par un AccountManager
	* @return double
	*/
	public double balance(){
		double balance = 0;
		for(PersonAccountTuple pat : this.listPerson){
			balance += pat.balance();
		}
		return balance;
	}
	
	/**
	 * Calcule le nombre de personne gérée par un AccountManager
	 * @return le total des personnes  gérée par un Accountmanager
	 */
	public int getNumberOfPersons(){
		return this.listPerson.size();
	}
	
	/**
	 * Calcule le nombre de comptes gérés par un AccountManager
	 * @return le total des comptes gérés par un AccountManager
	 */
	public int getNumberOfAccounts(){
		int nombreComptes = 0;
		for(PersonAccountTuple pat : this.listPerson){
			nombreComptes += pat.getAccounts().length;
		}
		return nombreComptes;
	}
	
	/**
	 * Affichage textuelle de l'objet AccountManager en affichant toutes les personnes et les comptes détenus par cet objet
	 * @return une représentation textuelle de l'AccountManager
	 */
	public String toString(){
		String expression = "AccountManager";
		for(PersonAccountTuple pat : this.listPerson){
			expression += "\n" + pat.toString();
		}
		return expression;
	}
	
	
}
