package bank.main;

public class TestMain {
	
	
	public static void main(String[] args) {
		
		//Test du type Person
		Person homme1 = new Person("AITABBI", "Sami", 1993, 1, 19);
		System.out.println(homme1.toString());
		System.out.println("Age : " + homme1.getAge());
		System.out.println("Nom : " + homme1.getName());
		System.out.println("Prénom : " + homme1.getSurname());
		
		Person homme2 = new Person("HERAULT", "Leo", 1993, 11, 19);
		System.out.println(homme2.toString());
		System.out.println("Age : " + homme2.getAge());
		System.out.println("Nom : " + homme2.getName());
		System.out.println("Prénom : " + homme2.getSurname());
		
		
		//Test de la classe Account
		Account compte1 = new Account("89654", 8000);
		compte1.deposit(2000);
		compte1.withdraw(5000);
		System.out.println(compte1.balance());
		System.out.println(compte1.accountNumber());
		compte1.withdraw(6000);
		System.out.println(compte1.toString());
		
		//Test de la classe LimitedAccount
		LimitedAccount compte2Limited = new LimitedAccount("89655", 8000, 200);
		compte2Limited.deposit(2000);
		compte2Limited.withdraw(5000);
		System.out.println(compte2Limited.balance());
		System.out.println(compte2Limited.accountNumber());
		compte2Limited.withdraw(5100);
		System.out.println(compte2Limited.toString());
		
		System.out.println(compte2Limited.getMaxCredit());
		compte2Limited.setMaxCredit(300);
		System.out.println(compte2Limited.getMaxCredit());
		compte2Limited.withdraw(250);
		System.out.println(compte2Limited.toString());
		
		//Test de la classe ForeignCurrencyAccount
		ForeignCurrencyAccount  compte3ForeignCurr = new ForeignCurrencyAccount("89656", 8000, 0.9);
		compte3ForeignCurr.deposit(2000);
		compte3ForeignCurr.withdraw(5000);
		System.out.println(compte3ForeignCurr.balance());
		System.out.println(compte3ForeignCurr.accountNumber());
		compte3ForeignCurr.withdraw(6000);
		System.out.println(compte3ForeignCurr.toString());
		
		
		System.out.println(compte3ForeignCurr.getExchangeRate());
		System.out.println(compte3ForeignCurr.balanceForeignCurrency());
		compte3ForeignCurr.depositForeignCurrency(5000);
		compte3ForeignCurr.withdrawForeignCurrency(20000);
		System.out.println(compte3ForeignCurr.toString());
		compte3ForeignCurr.withdrawForeignCurrency(200);
		System.out.println(compte3ForeignCurr.toString());
		
		//Test de PersonAccountTuple
		PersonAccountTuple pat1 = new PersonAccountTuple(homme1);
		pat1.addAccount(compte1);
		pat1.addAccount(compte2Limited);
		pat1.addAccount(compte3ForeignCurr);
		System.out.println(pat1.hasPersonAccount());
		System.out.println(pat1.toString());
		
		pat1.deleteAccount(compte3ForeignCurr);
		System.out.println(pat1.hasPersonAccount());
		System.out.println(pat1.toString());
		
		pat1.deleteAllAccounts();
		System.out.println(pat1.hasPersonAccount());
		System.out.println(pat1.toString());
		
		pat1.addAccount(compte1);
		pat1.addAccount(compte2Limited);
		
		System.out.println(pat1.ownedByPerson(homme2));
		System.out.println(pat1.balance());
		Account[] listeDeComptes= pat1.getAccounts();
		for (int i = 0; i < listeDeComptes.length ; i+= 1) {
			System.out.println(listeDeComptes[i].toString());
		}
		
		
		//Test de AccountManager
		AccountManager am = new AccountManager();
		am.manageAccount(homme1, compte1);
		System.out.println(am.toString() + "\n Nombre de personnes : " + am.getNumberOfPersons()
				+ "\n Nombre de comptes : " + am.getNumberOfAccounts()
				+ "\n Balance : " + am.balance() + "\n");
		am.manageAccount(homme1, compte2Limited);
		am.manageAccount(homme2, compte3ForeignCurr);
		System.out.println(am.toString() + "\n Nombre de personnes : " + am.getNumberOfPersons()
				+ "\n Nombre de comptes : " + am.getNumberOfAccounts()
				+ "\n Balance : " + am.balance() + "\n");
		am.unmanageAccount(homme1, compte2Limited);
		am.unmanageAccount(homme1, compte3ForeignCurr);
		System.out.println(am.toString() + "\n Nombre de personnes : " + am.getNumberOfPersons()
				+ "\n Nombre de comptes : " + am.getNumberOfAccounts()
				+ "\n Balance : " + am.balance() + "\n");
		am.unmanagePerson(homme2);
		System.out.println(am.toString() + "\n Nombre de personnes : " + am.getNumberOfPersons()
				+ "\n Nombre de comptes : " + am.getNumberOfAccounts()
				+ "\n Balance : " + am.balance() + "\n");
		
	}


}
