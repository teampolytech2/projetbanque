#!/bin/sh

#comme en python la première ligne de tout script indique au système
#le chemin pour trouver l'interpréteur
#comme en python (et tout langage de script un tant soit peu raisonnable),
#les commentaires commencent par un dièse

# pour générer l'archive contenant le code source
tar -jcvf projetPOO-src.tar.bz2 src/

# pour générer la javadoc
javadoc -classpath src/bank/main/*.java -d doc/

#pour compiler
mkdir build 2> /dev/null
javac -cp build/ src/bank/main/*.java -d build/
javac -cp build/ src/bank/oldtest/*.java -d build/

#Compilation des tests unitaires
javac -cp build/:/usr/share/java/junit4.jar src/bank/test/*.java -d build/

#Execution des tests unitaires
java -cp ./src/:build/:/usr/share/java/junit4.jar bank.test.MainTest


# pour produire le jar exécutable

jar cvmf META-INF/MANIFEST.MF projetPOO.jar -C build/ bank/main/ 


# 2> /dev/null indique que les messages d'erreur éventuels ne sont pas affichés

# pour l'exécuter plus tard :
java -jar projetPOO.jar

# pour nettoyer
#rm -fr doc/* build/* projetPOO.jar projetPOO-src.tar.bz2
