# La compilation et l'exécution des tests unitaires

## En ligne de commande

Pour pouvoir compiler les tests unitaires, on peut utiliser la commande de type :

```
javac -cp dossier1 dossier2 dossier3
```

L'option **-cp** (classpath) permet d'indiquer au programme javac où se trouvent les fichiers class nécessaires à la compilation des tests untitaires. Dans le cas de la commande présenté précedememnt, les fichiers class se trouvent dans le *dossier1*.  
Le *dossier2* correspond au dossier où se trouvent les fichiers java qui vont être compilés.  
Le *dossier3* correspond au dossier de destination des fichiers class crés par le programme javac.

Pour l'exécution des tests unitaires, on utilise la commande :
```
java -cp dossier1 dossier2 dossier3
```
L'option **-cp** se trouve pour l'exécution des tests unitaires et sert à indiquer le dossier où sont les fichiers class nécessaires à l'exécution des tests unitaires.

## Sous Eclipse

Dans l'explorateur de package, on effectue un clic droit sur le fichier java de la classe que l'on souhaite tester. Dans le menu contextuel, on sélectionne l'option `New > JUnit Test Case`. Par la suite, on choisit la version de test unitaire et le package dans lequel on veut mettre ce test unitaire.  
Pour compiler et exécuter le test unitaire, il faut dans le menu contextuel du clic droit sur le fichier java du test choisir l'option `Run As > JUnit test`.
