# Projet Java 1 partie 1 Utilisation de Git

## Git en local
              

* L'option –global sert à configurer globalement git. Si elle n'est pas utilisé git est configuré par la commande git congig uniquement 
pour le dépot dans lequel on se trouve.

citation de l'aide : 


>For writing options: write to global ~/.gitconfig file rather than the repository .git/config, write to $XDG_CONFIG_HOME/git/config file if >this file exists and the ~/.gitconfig file doesn’t.
>For reading options: read only from global ~/.gitconfig and from $XDG_CONFIG_HOME/git/config rather than from all available files.
>
>Status avant add de contenu.txt : fichier non suivi

>Status après add : nouveau fichier

* La commande git add envoie le fichier dans la zone d’attente (staging area)

citation de l'aide :

>This command updates the index using the current content found in the working tree, to prepare the content staged for the next commit. It >typically adds the current content of existing paths as a whole, but with some options it can also be used to add content with only part of >the changes made to the working tree files applied, or remove paths that do not exist in the working tree anymor

* La commande git commit envoie le fichier dans la zone immuable (la zone de sauvegarde
appelée dépôt local)

>Stores the current contents of the index in a new commit along with a log message from the user describing the changes.
>The content to be added can be specified in several ways:
>        1. by using git add to incrementally "add" changes to the index before using the commit command (Note: even modified files must be
>           "added");

>        2. by using git rm to remove files from the working tree and the index, again before using the commit command;

>        3. by listing files as arguments to the commit command, in which case the commit will ignore changes staged in the index, and instead
>           record the current content of the listed files (which must already be known to Git);

>        4. by using the -a switch with the commit command to automatically "add" changes from all known files (i.e. all files that are
>           already listed in the index) and to automatically "rm" files in the index that have been removed from the working tree, and then

>perform the actual commit;
>        5. by using the --interactive or --patch switches with the commit command to decide one by one which files or hunks should be part of >the commit, before finalizing the operation. See the “Interactive Mode” section of git-add(1) to learn how to operate these modes.
>The --dry-run option can be used to obtain a summary of what is included by any of the above for the next commit by giving the same set of >parameters (options and paths).
>If you make a commit and then find a mistake immediately after that, you can recover from it with git reset.

* Après le commit le status du fichier est “rien à valider, le copie de travail est proprre” (le fichier contenu.txt est bien dans la zone immuable)

* Après modification du fichier le status du fichier est “modifié”.

Message de git status :

>  Modifications qui ne seront pas validées :
>  (utilisez "git add <fichier>..." pour mettre à jour ce qui sera validé)
>  (utilisez "git checkout -- <fichier>..." pour annuler les modifications dans la copie de travail)

>    modifié:         contenu.txt

* Après avoir placé le fichier dans la zone immuable

Message de git status :

>[master 93c1c92] Sur la branche master Modifications qui seront validées :     modifié:         contenu.txt
> 1 file changed, 2 insertions(+)



* Après git add git status donne le message suivant :

>Modifications qui seront validées :
>  (utilisez "git reset HEAD <fichier>..." pour désindexer)

>    modifié:         contenu.txt


>Le git status après modification du fichier : 
>Sur la branche master
>Modifications qui seront validées :
>  (utilisez "git reset HEAD <fichier>..." pour désindexer)

>    modifié:         contenu.txt

> Modifications qui ne seront pas validées :
>  (utilisez "git add <fichier>..." pour mettre à jour ce qui sera validé)
>  (utilisez "git checkout -- <fichier>..." pour annuler les modifications dans la copie de travail)

>    modifié:         contenu.txt

* Pour comparer les différences entre deux commit il faut utiliser git diff <commit1> <commit2> (les références des commits sont accessibles avec git log).

* Pour comparer les dernières modifications d’un fichier qui n’ont pas encore été validées par un commit (entre la version de la zone d'attente (staged) et la version actuellement en zone immuable) il faut utiliser git diff.




* Les fichiers ajoutés par la commande git add sont **Bonjour.java** et **.gitignore**

* La commande pour nous l’indiquer est **git add -n** ou **git add -v**

* Pour désindexer un fichier sans le supprimer du répertoire de travail, il faut utiliser **git rm --cached**.

* Pour placer en zone immuable tous les fichiers déjà suivis par git il faut utiliser **git commit -n**.

*Pour créer un nouveau commit contenant tous les changements effectués sur les fichiers, il faut utiliser **git commit -a** 
 (git add n'est donc pas nécessaire avant un commit -a)

* Pour spécifier le message du commit dans la ligne de commande il faut utiliser l’option **-n**.

* Si le fichier est supprimé par la commande classique rm, le fichier est supprimé de l’ordinateur mais pas du dépot de git. 

* En utilisant la commande **git mv contenu2.txt contenu.txt** pour remettre la copie de contenu.txt à la place du fichier qui vient d’etre supprimée une erreur est obtenue :

>“fatal: pas sous le contrôle de version, source=contenu2.txt, destination=contenu.txt”

Cette erreur est due au fait que la copie contenu2.txt n’était pas indexée par git add

* L’option -p de git log donne les résultats de gitt -diff avant les commit réalisé donné.

> Without this flag, git log -p <path>...  shows commits that touch
>           the specified paths, and diffs about the same specified paths. With
>           this, the full diff is shown for commits that touch the specified
>           paths; this means that "<path>..." limits only commits, and doesn’t
>           limit diff for those commits.

>           Note that this affects all diff-based output types, e.g. those
>           produced by --stat, etc.



* L’option -3 de git log donne les 3 dernier commit:
> <revision range>
           Show only commits in the specified revision range. When no
           \<revision range\> is specified, it defaults to HEAD (i.e. the whole
           history leading to the current commit).  origin..HEAD specifies all
           the commits reachable from the current commit (i.e.  HEAD), but not
           from origin. For a complete list of ways to spell <revision range>,
           see the Specifying Ranges section of gitrevisions(7).


* Le dernier message de commit est modifié avec git commit --amend
* Les fichiers de la zone d’attente et d’archiffage se trouvent dans le répertoire .git/


## Git à distance

### Sur la machine1

* après git status le message suivant est obtenu:

>Sur la branche master
>Votre branche est à jour avec 'origin/master'.

>Modifications qui seront validées :
>  (utilisez "git reset HEAD <fichier>..." pour désindexer)

>    nouveau fichier: hw.sh

* Après le second git status on obtient le message suivant :
Sur la branche master
Votre branche est à jour avec 'origin/master'.

Modifications qui seront validées :
  (utilisez "git reset HEAD <fichier>..." pour désindexer)

    nouveau fichier: hw.sh

### Sur la machine 2 

* Après le première tentative de git push on obtient une erreur:

>fatal: Invalid gitfile format: super.sh
>fatal: Could not read from remote repository.



* La commande git push a permis de mettre à jour sur le dépot distant.

### Sur la machine 2 après modification de hw.sh et envoie dans la zone immuable sur les deux machine

* Le push sur la machine 2 (réalisé après celui sur la machine 1), donne le message d’erreur suivant :

>To https://github.com/SamiAITABBINAZI/GLTP1
> ! [rejected]        master -> master (fetch first)
>error: impossible de pousser des références vers 'https://github.com/SamiAITABBINAZI/GLTP1'
>astuce: Les mises à jour ont été rejetées car la branche distante contient du travail que
>astuce: vous n'avez pas en local. Ceci est généralement causé par un autre dépôt poussé
>astuce: vers la même référence. Vous pourriez intégrer d'abord les changements distants
>astuce: (par exemple 'git pull ...') avant de pousser à nouveau.
>astuce: Voir la 'Note à propos des avances rapides' dans 'git push --help' pour plus d'information.

* Après la commande git pull : La fusion entre le dépot distant et le dépot local ne fonctionne pas car il y a un conflit entre les 2 dépots.


>remote: Counting objects: 3, done.
>remote: Compressing objects: 100% (2/2), done.
>remote: Total 3 (delta 0), reused 3 (delta 0), pack-reused 0
>Unpacking objects: 100% (3/3), done.
>Depuis https://github.com/SamiAITABBINAZI/GLTP1
>   2241481..1e2caee  master     -> origin/master
>Fusion automatique de hw.sh
>CONFLIT (contenu) : Conflit de fusion dans hw.sh
>La fusion automatique a échoué ; réglez les conflits et validez le résultat.

* Message obtenu par la commande git status :

>Sur la branche master
>Votre branche et 'origin/master' ont divergé,
>et ont 1 et 1 commit différent chacune respectivement.
>  (utilisez "git pull" pour fusionner la branche distante dans la vôtre)

>Vous avez des chemins non fusionnés.
>  (réglez les conflits puis lancez "git commit")

>Chemins non fusionnés :
>  (utilisez "git add <fichier>..." pour marquer comme résolu)

>    modifié des deux côtés :hw.sh

>Fichiers non suivis:
>  (utilisez "git add <fichier>..." pour inclure dans ce qui sera validé)

>    hw.sh~
>    super.sh~

>aucune modification n'a été ajoutée à la validation (utilisez "git add" ou "git commit -a")

* Après fusion sur la machine 2 puis avoir effectuer un push et un clean avec git, la modification est bien récupérée sur la pmachine 2 avec un git pull.

## Suppression

* Pour supprimer le dépot local on peut utiliser **rm -rf .git\***

## Questions bonus

* Linus Torvalds pu faire ce choix de devoir placer ses fichiers en zone indexée  afin d'ajouter une "protection" avant de les valider sur le
dépôt local (commit) (validation en 2 étapes).

* pour éviter que git ne tracke les fichiers qui ne sont pas les fichiers source il faut crée un fichier .gitignore dans lequel on donne les extensions des types de fichier qu'on ne veut pas que git suive (ex "*~" et *.class")




